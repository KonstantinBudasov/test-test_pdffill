### Test task for PDFfill company.

### Autor: 
k.budasov@gmail.com

### Using:
- gulp
- SASS
- BEM methodology
- Media queries
- Graceful degradation (to IE8)
- Custom fonts

### To run
- Execute `npm install` from this directory to install dev dependencies.
- Execute `gulp` to run all tasks, launch the browser sync local server and watch for changes.

